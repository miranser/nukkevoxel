﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateQuads : MonoBehaviour
{
	
	//TODO - Turn to UVMapper material;
	//TODO - Combine Quads to one mesh;
	
	public Material cubeMaterial;
	
	public enum CubeSide{ Bottom, Top, Left, Right, Front, Back}
	
	void CreateQuad(CubeSide side)
	{

		var mesh = new Mesh();
		mesh.name = string.Format("{0}_{1}","ScriptedMesh",side.ToString());
		
		//Mesh parts:
		
		var vertices = new Vector3[4];
		var normals = new Vector3[4];
		var UVs = new Vector2[4];
		var triangles = new int[6];
		
		//UVs:
		
		var uv00 = new Vector2(0f, 0f);
		var uv10 = new Vector2(1f, 0f);
		var uv01 = new Vector2(0f, 1f);
		var uv11 = new Vector2(1f, 1f);
		
		//Vertices:
		
		var p0 = new Vector3(-0.5f,	-0.5f,  0.5f);
		var p1 = new Vector3( 0.5f,	-0.5f,  0.5f);
		var p2 = new Vector3( 0.5f,	-0.5f, -0.5f);
		var p3 = new Vector3(-0.5f,	-0.5f, -0.5f);
		var p4 = new Vector3(-0.5f,	 0.5f,  0.5f);
		var p5 = new Vector3( 0.5f,	 0.5f,  0.5f);
		var p6 = new Vector3( 0.5f,	 0.5f, -0.5f);
		var p7 = new Vector3(-0.5f,	 0.5f, -0.5f);
		
		//

		switch (side)
		{
			case CubeSide.Bottom:
				vertices = new[] {p0, p1, p2, p3};
				normals = new[] {Vector3.down, Vector3.down, Vector3.down, Vector3.down};
				break;
			
			case CubeSide.Top:
				vertices = new[] {p7, p6, p5, p4};
				normals = new[]{Vector3.up, Vector3.up, Vector3.up, Vector3.up};
				break;
			
			case CubeSide.Left:
				vertices = new[] {p7, p4, p0, p3};
				normals = new[]{Vector3.left, Vector3.left, Vector3.left, Vector3.left};
				break;
			
			case CubeSide.Right:
				vertices = new[] {p5, p6, p2, p1};
				normals = new[]{Vector3.right, Vector3.right, Vector3.right, Vector3.right};
				break;
			
			case CubeSide.Front:
				vertices = new[] {p4, p5, p1, p0};
				normals = new[]{Vector3.forward, Vector3.forward, Vector3.forward, Vector3.forward};
				
				break;
			case CubeSide.Back:
				vertices = new[] {p6, p7, p3, p2};
				normals = new[]{Vector3.back, Vector3.back, Vector3.back, Vector3.back};
				break;
			
		}
		
		triangles = new[] {3, 1, 0, 3, 2, 1};
		UVs = new[] {uv11, uv01, uv00, uv10};
		
		
		
		// Attachment to Mesh:

		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.uv = UVs;
		mesh.triangles = triangles;
		
		mesh.RecalculateBounds();
		
		var quad = new GameObject("quad");
		quad.transform.SetParent(transform);
		var meshFilter = quad.AddComponent<MeshFilter>();
		meshFilter.mesh = mesh;
		var renderer = quad.AddComponent<MeshRenderer>();

		renderer.sharedMaterial = cubeMaterial;
	}

	
	public void CreateQube()
	{
		CreateQuad(CubeSide.Front);
		CreateQuad(CubeSide.Back);
		CreateQuad(CubeSide.Top);
		CreateQuad(CubeSide.Bottom);
		CreateQuad(CubeSide.Left);
		CreateQuad(CubeSide.Right);
		
	}
	private void Start()
	{
		CreateQube();
	}
}
